# Label converter

## Description

Turn HTML string into png for label printing

## Usage

See [wiki](https://gitlab.com/miicat/label-converter/-/wikis/) for full usage

### Basic usage

This will print generated png file in bytes in json list `label-converter --header "<p>I'm a header</p>" --body "<h1>I'm body</h1><p>Nice to meet you! :3</p>" --footer "<p>I'm a footer</p>" --width 240 --height 240 --debug`  
Output:

```text
[[73,69,78,68,174,66,96,130..]]
```

![Image of outputting png file](https://gitlab.com/miicat/label-converter/-/raw/master/example.png)

### Print help text

Simply run this `label-converter -h` or `label-converter --help` for long help.

## Author

[Miika Launiainen](https://gitlab.com/miicat)

## Donating

If you want to support me, feel free to use paypal
[![Donate](https://img.shields.io/badge/Donate-PayPal-green.svg)](https://paypal.me/miicat)

## License

This library is licensed under GPLv3
