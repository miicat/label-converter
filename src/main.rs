use base64::{engine::general_purpose, Engine as _};
use clap::{crate_version, Arg, ArgAction, Command};

fn main() {
    let matches = Command::new("label-converter")
        .version(crate_version!())
        .author("Miika Launiainen <miika@miicat.eu>")
        .about("Turn HTML string into png for label printing")
        .arg(
            Arg::new("header")
                .long("header")
                .action(ArgAction::Set)
                .help("HTML header"),
        )
        .arg(
            Arg::new("body")
                .long("body")
                .action(ArgAction::Set)
                .required(true)
                .help("HTML body"),
        )
        .arg(
            Arg::new("footer")
                .long("footer")
                .action(ArgAction::Set)
                .help("HTML footer"),
        )
        .arg(
            Arg::new("width")
                .short('W')
                .long("width")
                .action(ArgAction::Set)
                .required(true)
                .help("Width of the png"),
        )
        .arg(
            Arg::new("height")
                .short('H')
                .long("height")
                .action(ArgAction::Set)
                .required(true)
                .help("Height of the png"),
        )
        .arg(
            Arg::new("black")
                .long("black")
                .action(ArgAction::SetTrue)
                .help("Force black and white"),
        )
        .arg(
            Arg::new("base64")
                .long("base64")
                .action(ArgAction::SetTrue)
                .help("Encode the png in base64"),
        )
        .arg(
            Arg::new("debug")
                .long("debug")
                .action(ArgAction::SetTrue)
                .help("Save created labels to current folder"),
        )
        .get_matches();

    let base64 = &matches.get_flag("base64");

    let labels = label_converter::create(matches.into());

    // Print labels as bytes in Json list
    let json: String = match serde_json::to_string(&labels) {
        Ok(i) => {
            // Encode in base64, if wanted
            if *base64 {
                general_purpose::STANDARD.encode(i)
            } else {
                i
            }
        }
        _ => {
            panic!("Error creating json list");
        }
    };

    println!("{}", json);
}
